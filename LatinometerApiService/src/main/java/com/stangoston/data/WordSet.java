/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.data;

/**
 *
 * @author sgoston
 */
public class WordSet {
    private String dbWord;
    private String language;
    private String points;

    public String getDbWord() {
        return dbWord;
    }

    public void setDbWord(String dbWord) {
        this.dbWord = dbWord;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
