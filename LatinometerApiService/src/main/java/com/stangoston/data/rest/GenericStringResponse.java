/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.data.rest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sgoston
 */
public class GenericStringResponse {
    private List<String> value;

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }
    
    public void setValue(String value) {
        this.value = new ArrayList<String>();
        this.value.add(value);
    }
}
