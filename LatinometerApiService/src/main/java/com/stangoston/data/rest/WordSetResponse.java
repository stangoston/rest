/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.data.rest;

import com.stangoston.data.WordSet;
import java.util.List;

/**
 *
 * @author sgoston
 */
public class WordSetResponse {
    private List<WordSet> wordSets;
    private int unidentified = 0;
    private int excluded = 0;
    private int french = 0;
    private int germanic = 0;
    private int greek = 0;
    private int latinate = 0;
    private int mutts = 0;
    private int purebreds = 0;
    private double points;

    public List<WordSet> getWordSets() {
        return wordSets;
    }

    public void setWordSets(List<WordSet> wordSets) {
        this.wordSets = wordSets;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getUnidentified() {
        return unidentified;
    }

    public int getExcluded() {
        return excluded;
    }

    public int getFrench() {
        return french;
    }

    public int getGermanic() {
        return germanic;
    }

    public int getGreek() {
        return greek;
    }

    public int getLatinate() {
        return latinate;
    }

    public int getMutts() {
        return mutts;
    }

    public int getPurebreds() {
        return purebreds;
    }

    public void setUnidentified() {
        this.unidentified++;
    }

    public void setExcluded() {
        this.excluded++;
    }

    public void setFrench() {
        this.french++;
    }

    public void setGermanic() {
        this.germanic++;
    }

    public void setGreek() {
        this.greek++;
    }

    public void setLatinate() {
        this.latinate++;
    }

    public void setMutts() {
        this.mutts++;
    }

    public void setPurebreds() {
        this.purebreds++;
    }
}
