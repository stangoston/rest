/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.data;

/**
 *
 * @author sgoston
 */
public enum Word {
    Excluded("brown", "0"),
    French("red", ".25"),
    Germanic("green", "0"),
    Greek("blue", "1.25"),
    Latinate("purple", "1"),
    Mutts("yellow", ".25"),
    Purebreds("darkorange", "0"),
    Unidentified("black", "0)");
    
    private final String color;
    private final String points;
    
    Word(String color, String points){
        this.color = color;
        this.points = points;
    }

    public String getColor() {
        return color;
    }

    public String getPoints() {
        return points;
    }
}
