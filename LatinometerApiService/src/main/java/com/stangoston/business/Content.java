/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.business;

import com.stangoston.data.Word;
import com.stangoston.data.WordSet;
import com.stangoston.data.rest.WordSetResponse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sgoston
 */
public class Content extends AbstractParams {

    private static final Logger logger = Logger.getLogger(Content.class.getCanonicalName());

    public Content() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Content.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getCount() throws SQLException {
        try (Connection connection = DriverManager.getConnection(path, username, password)) {
            stmt = connection.createStatement();
            query = "SELECT count from count;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                return rs.getString("count");
            }
        }
        return "";
    }

    public WordSetResponse getTextResults(String text) throws SQLException {
        ArrayList<WordSet> words = new ArrayList<>();
        WordSetResponse response = new WordSetResponse();
        Double points = 0.0;
        //Make words with a "-" two words
        for (String input : text.replace("-", " ").split("\\s")) {
            WordSet word = new WordSet();
            //remove all non characters for db words
            String dbWord = input.toLowerCase().replaceAll("[^a-z]", "");
            word.setDbWord(dbWord);

            try (Connection connection = DriverManager.getConnection(path, username, password)) {
                stmt = connection.createStatement();
                query = "SELECT * from exact_match WHERE exact_match = '" + dbWord + "';";
                PreparedStatement ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                rs.setFetchSize(1000);         //how many parts of DB grabbed at a time
                if (rs.next()) {
                    //Purebreds[7] Mutts[6] Latinate[5] Greek[4] Germanic[3] French[2] Excluded[1] Unidentified[0]
                    int language = rs.getInt("language_id");
                    switch (language) {
                        case 1:
                            word.setLanguage(Word.Excluded.toString());
                            word.setPoints(Word.Excluded.getPoints());
                            response.setExcluded();
                            break;

                        case 2:
                            word.setLanguage(Word.French.toString());
                            word.setPoints(Word.French.getPoints());
                            response.setFrench();
                            points = points + .25;
                            break;

                        case 3:
                            word.setLanguage(Word.Germanic.toString());
                            word.setPoints(Word.Germanic.getPoints());
                            response.setGermanic();
                            break;
                        case 4:
                            word.setLanguage(Word.Greek.toString());
                            word.setPoints(Word.Greek.getPoints());
                            response.setGreek();
                            points = points + 1.25;
                            break;

                        case 5:
                            word.setLanguage(Word.Latinate.toString());
                            word.setPoints(Word.Latinate.getPoints());
                            response.setLatinate();
                            points = points + 1;
                            break;

                        case 6:
                            word.setLanguage(Word.Mutts.toString());
                            word.setPoints(Word.Mutts.getPoints());
                            response.setMutts();
                            points = points + .25;
                            break;

                        case 7:
                            word.setLanguage(Word.Purebreds.toString());
                            word.setPoints(Word.Purebreds.getPoints());
                            response.setPurebreds();
                            break;

                        default:
                            System.out.println("Invalid language_id");
                            break;
                    }

                } else {
                    word.setLanguage(Word.Unidentified.toString());
                    word.setPoints(Word.Unidentified.getPoints());
                    response.setUnidentified();
                    query = "SELECT * from unidentified WHERE unidentified = \"" + dbWord + "\";";
                    rs = stmt.executeQuery(query);
                    rs.setFetchSize(50);
                    if (!rs.next()) {
                        query = "INSERT INTO unidentified (unidentified) VALUES (\"" + dbWord + "\");";
                        stmt.execute(query);
                    }
                }
            }
            words.add(word);
        }
        updateCount();
        response.setPoints(points);
        response.setWordSets(words);
        return response;
    }

    private void updateCount() throws SQLException {
        int num = Integer.parseInt(getCount());
        try (Connection connection = DriverManager.getConnection(path, username, password)) {
            stmt = connection.createStatement();
            query = "UPDATE count SET count=" + ++num + " WHERE id=\"1\";";
            stmt.execute(query);
        }
    }
}
