/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.application.context;

import com.stangoston.business.Content;
import org.apache.cxf.jaxrs.ext.ContextProvider;
import org.apache.cxf.message.Message;

/**
 *
 * @author sgoston
 */
public class ContentContextProvider implements ContextProvider<Content>{

    @Override
    public Content createContext(Message message) {
        return new Content();
    }
    
}
