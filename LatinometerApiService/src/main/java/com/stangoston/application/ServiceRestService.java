/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.application;

import com.stangoston.business.Content;
import com.stangoston.data.rest.GenericStringResponse;
import com.stangoston.data.rest.WordSetResponse;
import java.sql.SQLException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author sgoston
 */
@Path("/service")
@Produces(MediaType.APPLICATION_JSON)
public class ServiceRestService {

    @GET
    @Path("/count")
    public GenericStringResponse timesUsed(@Context Content content) throws SQLException {
        GenericStringResponse response = new GenericStringResponse();
        response.setValue(content.getCount());
        return response;
    }

    @GET
    @Path("/results/{text}")
    public WordSetResponse getTextResults(@Context Content content,
            @PathParam("text") String text) throws SQLException {
        return content.getTextResults(text);
    }
}
