/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.application.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author sgoston
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class GenericFaultMapper implements ExceptionMapper<Exception> {

    private static final Logger logger = Logger.getLogger(GenericFaultMapper.class.getCanonicalName());

    @Override
    public Response toResponse(Exception exception) {
        logger.log(Level.SEVERE, exception.getMessage(), exception);
        GenericExceptionResponse responseObj = GenericExceptionResponse.build(exception);
        return Response.serverError().entity(responseObj).build();
    }
}
