/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stangoston.application.exceptions;

import org.apache.commons.lang.exception.ExceptionUtils;

/**
 *
 * @author sgoston
 */
public class GenericExceptionResponse {
    private String message;
    private String details;
    private String type;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public static GenericExceptionResponse build(Exception e){
        GenericExceptionResponse response = new GenericExceptionResponse();
        String msg = e.getMessage();
        if(msg == null){
            msg = "Unknown Error";
        }
        response.setMessage(msg);
        response.setType(e.getClass().getCanonicalName());
        response.setDetails(ExceptionUtils.getFullStackTrace(e));
        return response;
    }
}
